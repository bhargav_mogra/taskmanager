package com.bhargavms.taskmanager;

public interface TasksListPresenter {
    void takeView(TasksListView view);
}
