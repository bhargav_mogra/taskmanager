package com.bhargavms.taskmanager;

public class TasksListPresenterImpl implements TasksListPresenter {
    private TasksListView mView;

    @Override
    public void takeView(TasksListView view) {
        mView = view;
    }
}
