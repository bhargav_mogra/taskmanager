package com.bhargavms.taskmanager;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

public class TaskDetailFragment extends Fragment {
    private static final String ARG_TASK_PRIMARY_KEY = "task";
    public static final int REQUEST_IMAGE_CAPTURE = 1324;

    private TaskModel mTask;
    private Realm mRealm;
    EditText titleView;
    EditText taskBodyView;
    List<RealmString> imagePaths = new ArrayList<>();
    Calendar reminderDateTime;
    LinearLayout thumbnailHolder;
    FloatingActionButton btnSave;

    public TaskDetailFragment() {
        // Required empty public constructor
    }

    public static TaskDetailFragment newInstance(long taskPrimaryKey) {
        TaskDetailFragment fragment = new TaskDetailFragment();
        if (taskPrimaryKey != 0) {
            Bundle args = new Bundle();
            args.putLong(ARG_TASK_PRIMARY_KEY, taskPrimaryKey);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
        if (getArguments() != null) {
            long createdAt = getArguments().getLong(ARG_TASK_PRIMARY_KEY);
            mTask = mRealm.where(TaskModel.class).equalTo(TaskModel.FIELD_CREATED_AT, createdAt)
                    .findFirst();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task_detail, container, false);
        titleView = (EditText) view.findViewById(R.id.detail_task_title);
        taskBodyView = (EditText) view.findViewById(R.id.detail_task_body);
        btnSave = (FloatingActionButton) view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick(v);
            }
        });
        Button btnSetReminder = (Button) view.findViewById(R.id.btn_set_reminder);
        btnSetReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSetReminderClicked(v);
            }
        });
        ImageButton btnTakePic = (ImageButton) view.findViewById(R.id.btn_take_pic);
        btnTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTakePicClicked(v);
            }
        });
        thumbnailHolder = (LinearLayout) view.findViewById(R.id.images_holder);
        if (mTask != null) {
            titleView.setText(mTask.getTitle());
            taskBodyView.setText(mTask.getDetails());
            if (mTask.getReminderDateTime() != 0) {
                reminderDateTime = Calendar.getInstance();
                reminderDateTime.setTime(new Date(
                        mTask.getReminderDateTime()
                ));
            }
            imagePaths.addAll(mTask.getImagePaths());
            for (RealmString path : mTask.getImagePaths()) {
                thumbnailHolder.addView(createImageView(
                        BitmapFactory.decodeFile(path.getString())
                ));
            }
        }
        return view;
    }

    public void onTakePicClicked(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private ImageView createImageView(String path) {
        ImageView thumbnailView = new ImageView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        thumbnailView.setLayoutParams(params);
        thumbnailView.setImageURI(Uri.parse(path));
        return thumbnailView;
    }

    private ImageView createImageView(Bitmap bm) {
        ImageView thumbnailView = new ImageView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(bm.getWidth(), bm.getHeight());
        thumbnailView.setLayoutParams(params);
        thumbnailView.setImageBitmap(bm);
        return thumbnailView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            final Bitmap imageBitmap = (Bitmap) extras.get("data");
            if (imageBitmap == null)
                return;
            thumbnailHolder.addView(createImageView(imageBitmap));
            btnSave.setEnabled(false);
            Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    try {
                        subscriber.onNext(createImageFile(imageBitmap).getAbsolutePath());
                    } catch (IOException | NullPointerException e) {
                        subscriber.onError(e);
                    }
                }
            }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<String>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            btnSave.setEnabled(true);
                            Toast.makeText(getActivity(), "Failed to save image", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNext(String s) {
                            btnSave.setEnabled(true);
                            imagePaths.add(new RealmString(s));
                        }
                    });
        }
    }

    private File createImageFile(Bitmap bm) throws IOException {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/req_images");
        myDir.mkdirs();
        String fname = "Image-" + System.currentTimeMillis() + ".jpg";
        File file = new File(myDir, fname);
        FileOutputStream out = new FileOutputStream(file);
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }
        return file;
    }

    public void onSetReminderClicked(View v) {
        if (reminderDateTime == null)
            reminderDateTime = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                reminderDateTime.set(Calendar.YEAR, year);
                reminderDateTime.set(Calendar.MONTH, monthOfYear);
                reminderDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        reminderDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        reminderDateTime.set(Calendar.MINUTE, minute);
                        reminderDateTime.set(Calendar.SECOND, 5);
                    }
                };
                TimePickerDialog timePicker = new TimePickerDialog(getActivity(),
                        listener, reminderDateTime.get(Calendar.HOUR_OF_DAY),
                        reminderDateTime.get(Calendar.MINUTE),
                        true);
                timePicker.show();
            }
        };
        new DatePickerDialog(getActivity(), dateSetListener,
                reminderDateTime.get(Calendar.YEAR), reminderDateTime.get(Calendar.MONTH),
                reminderDateTime.get(Calendar.DAY_OF_MONTH)).show();
    }


    public void onSaveClick(View view) {
        String title = titleView.getText().toString();
        String taskBody = taskBodyView.getText().toString();
        if (title.isEmpty()) {
            Toast.makeText(getActivity(), "Task name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        long createdAt;
        if (mTask != null) {
            NotificationPublisher.removeNotification(getActivity(), mTask.getTitle(), mTask.getCreatedAt());
            createdAt = mTask.getCreatedAt();
        } else {
            createdAt = System.currentTimeMillis();
        }
        mRealm.beginTransaction();
        mRealm.insertOrUpdate(new TaskModel(title, taskBody,
                imagePaths,
                reminderDateTime != null ? reminderDateTime.getTimeInMillis() : 0,
                createdAt));
        mRealm.commitTransaction();
        if (reminderDateTime != null)
            NotificationPublisher.sendNotificationAt(
                    reminderDateTime.getTimeInMillis(),
                    getActivity(),
                    title, createdAt);
        getActivity().finish();
    }


    @Override
    public void onDetach() {
        super.onDetach();

        mRealm.close();
    }
}
