package com.bhargavms.taskmanager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.Sort;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TaskFragment extends Fragment {
    private OnListFragmentInteractionListener mListener;
    private ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper
            .SimpleCallback(ItemTouchHelper.LEFT, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            if (viewHolder instanceof TaskRecyclerViewAdapter.ViewHolder) {
                TaskRecyclerViewAdapter.ViewHolder vh = (TaskRecyclerViewAdapter.ViewHolder) viewHolder;
                mRealm.beginTransaction();
                String title = vh.mItem.getTitle();
                long id = vh.mItem.getCreatedAt();
                if (vh.mItem.isValid())
                    vh.mItem.deleteFromRealm();
                mRealm.commitTransaction();
                NotificationPublisher.removeNotification(getActivity(), title, id);
            }
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return false;
        }
    };
    ItemTouchHelper helper = new ItemTouchHelper(simpleCallback);
    private Realm mRealm;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TaskFragment() {
    }

    public static TaskFragment newInstance() {
        return new TaskFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        OrderedRealmCollection<TaskModel> data = mRealm.where(TaskModel.class)
                .equalTo(TaskModel.FIELD_DELETED, false)
                .findAll().sort(TaskModel.FIELD_REMINDER_AT, Sort.ASCENDING,
                        TaskModel.FIELD_CREATED_AT, Sort.ASCENDING);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            TaskRecyclerViewAdapter mAdapter = new TaskRecyclerViewAdapter(getActivity(), data,
                    mListener);
            recyclerView.setAdapter(mAdapter);
            helper.attachToRecyclerView(recyclerView);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mRealm.close();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListItemTap(TaskModel task);
    }
}
