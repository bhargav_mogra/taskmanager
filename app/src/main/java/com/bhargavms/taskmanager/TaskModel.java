package com.bhargavms.taskmanager;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TaskModel extends RealmObject {
    static final String FIELD_TITLE = "title";
    static final String FIELD_DELETED = "deleted";
    static final String FIELD_CREATED_AT = "createdAt";
    static final String FIELD_REMINDER_AT = "reminderDateTime";
    private String title;
    private String details;
    private RealmList<RealmString> imagePaths;
    private long reminderDateTime;
    @PrimaryKey
    private long createdAt;
    private boolean deleted = false;

    public TaskModel() {
    }

    public TaskModel(String title, String details, List<RealmString> imagePaths,
                     long reminderDateTime, long createdAt) {
        this.title = title;
        this.details = details == null ? "" : details;
        if (imagePaths != null)
            this.imagePaths = new RealmList<>((RealmString[]) imagePaths.toArray(new RealmString[0]));
        this.reminderDateTime = reminderDateTime;
        this.createdAt = createdAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public RealmList<RealmString> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(RealmList<RealmString> imagePaths) {
        this.imagePaths = imagePaths;
    }

    public long getReminderDateTime() {
        return reminderDateTime;
    }

    public void setReminderDateTime(long reminderDateTime) {
        this.reminderDateTime = reminderDateTime;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
