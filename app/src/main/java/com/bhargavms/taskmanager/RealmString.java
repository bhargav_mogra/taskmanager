package com.bhargavms.taskmanager;

import io.realm.RealmObject;

/**
 * Created by bhargav on 10/21/16.
 */

public class RealmString extends RealmObject {
    private String string;

    public RealmString() {
    }

    public RealmString(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
