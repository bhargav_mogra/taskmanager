package com.bhargavms.taskmanager;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationPublisher extends BroadcastReceiver {
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";

    public static void sendNotificationAt(long timeInMillis, Context c, String title, long taskPrimaryKey) {
        if (timeInMillis < System.currentTimeMillis())
            return;
        c = c.getApplicationContext();
        PendingIntent pendingIntent = createPendingIntent(
                c,
                getNotification(title, taskPrimaryKey, c),
                taskPrimaryKey
        );
        AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
    }

    public static void removeNotification(Context c, String title, long taskPrimaryKey) {
        c = c.getApplicationContext();
        PendingIntent pendingIntent = createPendingIntent(
                c,
                getNotification(title, taskPrimaryKey, c),
                taskPrimaryKey
        );
        AlarmManager am = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    private static Notification getNotification(String title, long taskPrimaryKey, Context c) {
        Notification.Builder builder = new Notification.Builder(c);
        builder.setContentTitle(title);
        builder.setContentIntent(createPendingIntent(c, taskPrimaryKey));
        builder.setSmallIcon(R.drawable.ic_adb_black_24dp);
        return builder.build();
    }

    private static PendingIntent createPendingIntent(Context c, long taskPrimaryKey) {
        Intent resultIntent = TaskDetailsActivity.getStartIntent(c, taskPrimaryKey);
        return PendingIntent.getActivity(c,
                0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent createPendingIntent(Context c, Notification notification, long id) {
        Intent notificationIntent = new Intent(c, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        return PendingIntent.getBroadcast(c, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        long id = intent.getLongExtra(NOTIFICATION_ID, 0);
        notificationManager.notify((int) id, notification);
    }
}
