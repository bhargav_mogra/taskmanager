package com.bhargavms.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TaskDetailsActivity extends AppCompatActivity {
    private static final String EXTRAS_TASK_PRIMARY_KEY = "taskPrimaryKey";

    public static void start(Context c, long taskPrimaryKey) {
        Intent i = new Intent(c, TaskDetailsActivity.class);
        i.putExtra(EXTRAS_TASK_PRIMARY_KEY, taskPrimaryKey);
        c.startActivity(i);
    }

    public static Intent getStartIntent(Context c, long taskPrimaryKey) {
        Intent i = new Intent(c, TaskDetailsActivity.class);
        i.putExtra(EXTRAS_TASK_PRIMARY_KEY, taskPrimaryKey);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        long primaryKey = getIntent().getExtras().getLong(EXTRAS_TASK_PRIMARY_KEY);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_task_details,
                TaskDetailFragment.newInstance(primaryKey)).commit();
    }
}
