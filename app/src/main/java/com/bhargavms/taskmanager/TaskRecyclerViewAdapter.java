package com.bhargavms.taskmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;


public class TaskRecyclerViewAdapter extends
        RealmRecyclerViewAdapter<TaskModel, TaskRecyclerViewAdapter.ViewHolder> {

    private final TaskFragment.OnListFragmentInteractionListener mListener;

    public TaskRecyclerViewAdapter(Context c, OrderedRealmCollection<TaskModel> items,
                                   TaskFragment.OnListFragmentInteractionListener listener) {
        super(c, items, true);
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        TaskModel item = getData().get(position);
        holder.mItem = item;
        holder.mTitleView.setText(item.getTitle());
        if (item.getReminderDateTime() != 0)
            holder.mReminderDateView.setText(
                    DateUtils.getRelativeTimeSpanString(item.getReminderDateTime())
            );
        else
            holder.mReminderDateView.setText("");
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListItemTap(holder.mItem);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final TextView mReminderDateView;
        public TaskModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.task_title);
            mReminderDateView = (TextView) view.findViewById(R.id.reminder_date_time);
        }

        @Override
        public String toString() {
            return mItem.getTitle();
        }
    }
}
